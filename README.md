### UPC\+\+: a PGAS extension for C\+\+ ###

UPC++ is a parallel programming extension for developing C++ applications with the partitioned global address space (PGAS) model.  UPC++ has three main objectives: 

* Provide an object-oriented PGAS programming model in the context of the popular C++ language
* Add useful parallel programming idioms unavailable in Unified Parallel C (UPC), such as asynchronous remote function invocation and multidimensional arrays, to support complex scientific applications
* Offer an easy on-ramp to PGAS programming through interoperability with other existing parallel programming systems (e.g., MPI, OpenMP, CUDA)

### Media ###

UPC++ enabled new seismic research insights

* [Origins of Volcanic Island Chains](https://youtu.be/tCphzt8iaWc)
* [CT Scan of Earth Links Mantle Plumes with Volcanic Hotspots](http://cs.lbl.gov/news-media/news/2015/ct-scan-of-earth-links-mantle-plumes-with-volcanic-hotspots/)

Please see the [UPC++ wiki](https://bitbucket.org/upcxx/upcxx/wiki) for more info.